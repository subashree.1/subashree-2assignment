package Login;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Assignment {

    public WebDriver driver;

    @BeforeClass
    public void browserLaunch() {
        driver = new ChromeDriver();
        driver.get("https://admin-demo.nopcommerce.com/login");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @Test (priority = 1)
    public void login() {
        driver.findElement(By.xpath("//button[text()='Log in']")).click();
        String actual = driver.findElement(By.xpath("//a[text()='John Smith']")).getText();
        String expected ="John Smith";
        Assert.assertEquals(actual, expected);
    }
    @Test (priority = 2)
    public void Categories() throws IOException {
          driver.findElement(By.xpath("//i[@class='nav-icon fas fa-book']")).click();
          driver.findElement(By.partialLinkText("Categories")).click();
          driver.findElement(By.partialLinkText("Add new")).click();
          
          File file1 = new File("/home/subashree/Documents/Ass.xlsx");
          FileInputStream fis1 = new FileInputStream(file1);
          XSSFWorkbook wbook1 = new XSSFWorkbook(fis1);
          XSSFSheet st = wbook1.getSheet("Sheet1");
          int rows = st.getLastRowNum();
          
          for (int i = 1; i <= rows; i++) {
              String Name = st.getRow(i).getCell(0).getStringCellValue();
              String Description = st.getRow(i).getCell(1).getStringCellValue();
               double Price1 = st.getRow(i).getCell(2).getNumericCellValue();
               String PriceFrom = String.valueOf(Price1);
               double Price2 = st.getRow(i).getCell(3).getNumericCellValue();
               String PriceTo = String.valueOf(Price2);
               driver.findElement(By.id("Name")).sendKeys(Name);
               driver.switchTo().frame("Description_ifr");
               driver.findElement(By.xpath("//br[@data-mce-bogus=\"1\"]")).sendKeys(Description);
               driver.switchTo().defaultContent();
               WebElement downtm = driver.findElement(By.xpath("(//label[@class=\"col-form-label\"])[13]"));
               
               JavascriptExecutor js = (JavascriptExecutor)driver;
               js.executeScript("arguments[0].scrollIntoView(true)",downtm);
               
               driver.findElement(By.xpath("//div[text()='SEO']")).click();
               
               WebElement savebutton = driver.findElement(By.xpath("//button[@name='save']"));
               js.executeScript("arguments[0].scrollIntoView(false)", savebutton);
               savebutton.click();
               }
           driver.findElement(By.id("SearchCategoryName")).sendKeys("SGgu");
           driver.findElement(By.id("search-categories")).click();
           WebElement ele2 = driver.findElement(By.xpath("//td[text()='SGgu']"));
           String exptName = ele2.getText();
          Assert.assertEquals("SGgu", exptName);
    }
          @Test( priority =3)
          public void product() {
              
              driver.findElement(By.xpath("//p[contains(text(),'Products')]")).click();
              driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
              WebElement cat = driver.findElement(By.id("SearchCategoryId"));
              Select sel = new Select(cat);
              sel.selectByValue("2");
              driver.findElement(By.id("search-products")).click();
              WebElement ele3 = driver.findElement(By.xpath("//td[text()='Build your own computer']"));
              String Product = ele3.getText();
              Assert.assertEquals("Build your own computer", Product);
              
          }
          @Test ( priority =3)
          public void Manufacturers() throws IOException {
              
              driver.findElement(By.xpath("//p[contains(text(),'Manufacturers')]")).click();
              driver.findElement(By.xpath("//a[@class=\"btn btn-primary\"]")).click();
              
              File file2 = new File("/home/subashree/Documents/Ass.xlsx");
                FileInputStream fis2 = new FileInputStream(file2);
                XSSFWorkbook wbook2 = new XSSFWorkbook(fis2);
                XSSFSheet st = wbook2.getSheet("Sheet2");
                
                int rows = st.getLastRowNum();
                
                for (int i = 1; i <= rows; i++) {
                    String Name1 = st.getRow(i).getCell(0).getStringCellValue();
                    String Description1 = st.getRow(i).getCell(1).getStringCellValue();
                    
                    driver.findElement(By.id("Name")).sendKeys(Name1);
                    driver.switchTo().frame("Description_ifr");
                    driver.findElement(By.xpath("//br[@data-mce-bogus=\"1\"]")).sendKeys(Description1);
                    driver.switchTo().defaultContent();
                    
                    driver.findElement(By.xpath("//i[@class='far fa-save']")).click();
                    }
                
                driver.findElement(By.id("SearchManufacturerName")).sendKeys("Computers");
                driver.findElement(By.id("search-manufacturers")).click();
                
                WebElement ele4 = driver.findElement(By.xpath("//td[text()=' Computers']"));
                String manu = ele4.getText();
                Assert.assertEquals(" Computers", manu);
                
          }

    
          
     
          
          
          
      
    
    

    @AfterClass
    public void closeBrowser() {

    }

}